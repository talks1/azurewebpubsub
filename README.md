
# Azure Web Pub Sub Example

This is an example for how to have a static site which connects to an Azure Web Pub Sub resource via Web Sockets.
An azure function can broadcast events to the client by using a `WebPubSubServiceClient`



Originally created this after reading [this article](https://docs.microsoft.com/en-us/azure/azure-web-pubsub/tutorial-pub-sub-messages?tabs=csharp).


On the server side this is the important stuff
```
import { WebPubSubServiceClient } from '@azure/web-pubsub'

pubsubService = new WebPubSubServiceClient(config.pubsub.connectionString, pubsubChannel);

const event: IProcessingEvent = { message: `Time on server ${new Date()}` }
await pubsubService.sendToAll(event);    
```