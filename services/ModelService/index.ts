import { createAzureFunctionHandler } from "azure-function-express";
import bodyparser from "body-parser";
import Debug from "debug";
import express from "express";
import passport from "passport";
import { authorizationStrategy } from "../src/auth";
import { config } from "../src/config";
import { WebPubSubServiceClient } from '@azure/web-pubsub'
import {IProcessingEvent} from '../../application/src/types'

const debug = Debug("ModelService");

const pubsubChannel ='stream'

const app = express();
app.use(bodyparser.urlencoded({ extended: true }));
app.use(passport.initialize());
passport.use(authorizationStrategy(config));

let pubsubService = null
const initServices = async () => {
  pubsubService = new WebPubSubServiceClient(config.pubsub.connectionString, pubsubChannel);
};

  
app.get('/api/ModelService/negotiate', async (req, res) => {
  if (!pubsubService) await initServices();     
  let token = await pubsubService.getClientAccessToken({
    roles: ['webpubsub.sendToGroup.stream', 'webpubsub.joinLeaveGroup.stream']
  });
  res.send({
    url: token.url
  });
  // res.status(200).send({});
});
  
app.get('/api/ModelService/publish', async (req, res) => {
    if (!pubsubService) await initServices();     
    console.log('--------Publish')
    const event: IProcessingEvent = { message: `Time on server ${new Date()}` }
    await pubsubService.sendToAll(event);    
    res.status(200).send({});    
  });

export default createAzureFunctionHandler(app);

