import React, { FC, useEffect, useState } from 'react'
import { getPubSub, publish } from '../contract/api'

export const Messages: FC = () => {

  const [broadcast,broadcastSet] = useState('')

  const handleAction = async ()=>{
    await publish()
  }
 
  useEffect(() => {
    const getData = async () => {
      const result: any = await getPubSub()            
      let ws = new WebSocket(result.url);
      ws.onopen = () => console.log('connected');
      ws.onmessage = (event) => {        
        const data = JSON.parse(event.data)
        broadcastSet(data.message);
      }
    }
    getData()
  }, [])

  console.log(broadcast)
  
  return (
    <div className='pageContainer'>      
      <button onClick={handleAction}>Broadcast message</button>
      <div>
        {broadcast}
      </div>
    </div>
  )
}
