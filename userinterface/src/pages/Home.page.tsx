import React, { FC, useState } from 'react'

import Background from '../assets/data-routing.jpg'
import Logo from '../assets/logo.png'

export const Home: FC = () => {
  const backgroundImage = Background

  return (
    <div>
      <div className='login-page' style={{ backgroundImage: `url(${backgroundImage})` }}>
        <div className='login-gradient' />
        <div className='login-container'>
          <div className='login-header'>
            <img src={Logo} alt='' width='300px' />
          </div>
          <div className='login-content'>
            <h2>Welcome to Dataset Routing </h2>
            <p>This Aurecon application allows configuration of Data Event Routing.</p>
            <div className='login-content' />
          </div>
        </div>
      </div>
    </div>
  )
}
