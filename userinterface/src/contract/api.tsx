/* eslint-disable */
import { COMMAND_STATUS } from '../../../application/src/domain'
import { ICommandResult, IDatasetRouting } from '../../../application/src/types'
import { getAPI } from '../config'

const handleResponse = (res: Response) => {
  try {
    if (res.status === 401) {
      window.location.href = `/`
    } else if (res.status === 500) {
      return null
    } else {
      return res !== null ? res.json() : []
    }
  } catch (ex) {
    console.log(ex)
    return []
  }
}

export const headersWithToken = (token: string | undefined): HeadersInit => {
  return {
    Authorization: `Bearer ${token}`,
    'Content-Type': 'application/json',
  }
}

export interface IAPIRequest<T, U> {
  (props: T): Promise<U>
}

export function TokenExpiryWrapper<T, U>(
  apiCall: IAPIRequest<T, U>,
  // scopes: string[],
  // errorReturnValue: U,
): IAPIRequest<T, U> {
  //does nothing but could refresh the api token
  //const localToken = window.localStorage.getItem('AccessToken')
  return apiCall
}

export interface IResponse {
  success: boolean
  message?: string
  data?: any
}

// eslint-disable-file
const service = {
  get: (url: string, options: object) =>
    fetch(getAPI() + url, { ...options /*credentials: 'include'*/ }).then(handleResponse),
  post: (url: string, body: object, options: object = {}) =>
    fetch(getAPI() + url, {
      ...options,
      /*credentials: 'include',*/ method: 'POST',
      body: JSON.stringify(body),
    }).then(handleResponse),
  put: (url: string, body: object, options: object = {}) =>
    fetch(getAPI() + url, {
      ...options,
      /*credentials: 'include',*/ method: 'PUT',
      body: JSON.stringify(body),
    }).then(handleResponse),
  delete: (url: string, options: object = {}) =>
    fetch(getAPI() + url, {
      ...options,
      /*credentials: 'include',*/ method: 'DELETE',
    }).then(handleResponse),
}

export async function getApplicationConfig(): Promise<any> {
  const localToken = window.localStorage.getItem('AccessToken')
  return service.get(`/ConfigService`, {
    headers: { Authorization: `Bearer ${localToken}` },
  })
}


export async function getPubSub() : Promise<ICommandResult> { 
  const localToken = window.localStorage.getItem('AccessToken')  
  return service.get(`/ModelService/negotiate`, {
    headers: { Authorization: `Bearer ${localToken}` },
  })
}

export async function publish() : Promise<ICommandResult> { 
  const localToken = window.localStorage.getItem('AccessToken')  
  return service.get(`/ModelService/publish`, {
    headers: { Authorization: `Bearer ${localToken}` },
  })
}