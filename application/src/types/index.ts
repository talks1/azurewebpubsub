
export interface ICommandResult {
  status: string
  msg: string
  entity?: any // eslint-disable-line 
}

export interface IProcessingEvent {
  message: string
}