import { formatWithOptions } from 'date-fns/fp';
import { eo } from 'date-fns/locale';

export const formatEpoch = formatWithOptions({ locale: eo }, 'yyyy-MM-dd HH:mm')