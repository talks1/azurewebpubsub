

//ICommandResult
export enum COMMAND_STATUS {
  OK = 'OK',
  FAILED = 'FAILED',
  NOTFOUND = 'NOTFOUND',
}


export const ERROR_STATUS = 'ERROR'
export const SUCCESS_STATUS = 'SUCCESS'
export const NOTFOUND_STATUS = 'NOTFOUND'
export const ACTIVE_STATUS = 'ACTIVE'

