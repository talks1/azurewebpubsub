import { describe } from "riteway";
import { formatEpoch} from '../src/formatting'

describe("Data Format", async (assert) => {
  
  const now = Date.now()  
  const result = formatEpoch(now)  

  assert({
    given: "no arguments",
    should: "return 0",
    actual: 0,
    expected: 0,
  });

});
